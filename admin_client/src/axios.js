import axios from 'axios';
import { StatusCodes } from 'http-status-codes';


const instance = axios.create();

axios.interceptors.response.use(undefined, (error) => {
  if(error?.response?.status === StatusCodes.UNAUTHORIZED) {
    window.location.href = "/login";

    localStorage.removeItem('tt');
    instance.defaults.headers.common['Authorization'] = '';
    return;
  }
  return Promise.reject(error);
})

const token = localStorage.getItem('tt');
if(token) {
  instance.defaults.headers.common['Authorization'] = `Token ${token}`;

  if(window.location.pathname === '/login') {
    window.location.href = '/';
  }
} else {
  if(window.location.pathname !== '/login') {
    window.location.href = '/login';
  }
}

export default instance;
