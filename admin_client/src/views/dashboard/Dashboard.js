import {
  CRow,
} from '@coreui/react'

import React, { useEffect, } from 'react'

import SwapHistoryComponent from '../../components/dashboard/SwapHistory';
import RebalanceHistoryComponent from '../../components/dashboard/RebalanceHistory';
import BalanceComponent from '../../components/dashboard/Balance';
import VolumeComponent from '../../components/dashboard/Volume';
import VolumeChartComponent from '../../components/dashboard/MonthVolumeChart';

import {
  getSwapHistory,
  getBalance,
  getVolume,
  getVolumeChart,
  getDashboardRebalanceHistory,
} from '../../actions/dmm';
import {getThisMonth} from '../../utils/common';


const thisMonth = getThisMonth();

const Dashboard = () => {

  const refreshData = () => {
    getBalance();
    getVolume(thisMonth);
    getVolumeChart();
    getSwapHistory();
    getDashboardRebalanceHistory();
  };

  useEffect(()=>{
    refreshData();

    const interval = setInterval(refreshData, 60 * 1000);
    return () => {
      clearInterval(interval);
    }
  }, []);

  return (
    <>
      <BalanceComponent/>

      <VolumeComponent/>

      <VolumeChartComponent/>

      <CRow>

        <SwapHistoryComponent/>

        <RebalanceHistoryComponent/>

      </CRow>
    </>
  )
}

export default Dashboard
