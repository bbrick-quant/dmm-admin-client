import {
  CButton,
  CDataTable,
  CFormGroup,
  CFormText,
  CInput,
} from '@coreui/react';
import {
  CCard,
  CCardBody,
  CCardHeader,
  CRow,
  CCol,
} from '@coreui/react'


import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";

import {
  getCSVRebalanceHistory,
  getReportRebalanceHistory,
} from '../../actions/dmm';
import {
  number,
  handleDecimal,
  customDate,
  subMonth,
  yyyymmdd,
  addDay,
} from '../../utils/common';

const today = yyyymmdd(new Date())

const RebalanceHistory = () => {
  const list = useSelector(state => state.dmm.rebalanceHistory);

  const [start, setStart] = useState(subMonth(1));
  const [end, setEnd] = useState(today);

  const handleDate = (e) => {
    'start-date' === e.target.id ? setStart(e.target.value) : setEnd(e.target.value)
  }

  const search = () => {
    getReportRebalanceHistory(start, addDay(end, 1));
  }

  const csvDownload = () => {
    getCSVRebalanceHistory(start, addDay(end, 1))
  }

  useEffect(() => {
    search();
  }, []);

  const resetOptions = () => {
    setStart(subMonth(1))
    setEnd(today)
  }

  const handleStatus = (status) => {
    if ('S' === status) return 'SUCCEED'
    if ('F' === status) return 'FAILED'
    if ('C' === status) return 'CANCELED'
    if ('P' === status) return 'PROCESSING'
  }

  return (
    <>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              <h3>Rebalance History</h3>
              <hr/>

              <div>
                <CFormGroup row>
                  <CCol xs="12" md="2">
                    <CFormText className="help-block"><h6>Start Day</h6></CFormText>
                    <CInput
                      id="start-date"
                      type="date"
                      name="date-input"
                      value={start}
                      onChange={handleDate}
                    />
                  </CCol>
                  <CCol xs="12" md="2">
                    <CFormText className="help-block"><h6>End Day</h6></CFormText>
                    <CInput
                      id="end-date"
                      type="date"
                      name="date-input"
                      value={end}
                      onChange={handleDate}
                    />
                  </CCol>
                  <CCol xs="12" md="1" style={{maxWidth: '8%', marginTop: '27px'}}>
                    <CButton onClick={resetOptions} variant="outline" color="secondary">초기화</CButton>
                  </CCol>
                  <CCol xs="12" md="1" style={{marginTop: '27px'}}>
                    <CButton onClick={search} variant="outline" color="warning">검색</CButton>
                  </CCol>
                  <CCol xs="12" md="1" style={{marginTop: '27px'}}>
                    <CButton onClick={csvDownload} variant="outline" color="success">CSV</CButton>
                  </CCol>
                </CFormGroup>
              </div>
            </CCardHeader>
            <CCardBody>
              <CDataTable
                // hover
                striped
                bordered
                pagination
                itemsPerPage={10}
                items={list}
                fields={[
                  {key: 'id', label: 'id'},
                  {key: 'fromCoin', label: 'From'},
                  {key: 'fromAmount', label: 'From Amount'},
                  {key: 'toCoin', label: 'To'},
                  {key: 'toAmount', label: 'To Amount'},
                  {key: 'handleStatus', label: 'Status'},
                  {key: 'completedPlan', label: 'Completed Plan'},
                  {key: 'userName', label: 'User'},
                  {key: 'createdAt', label: 'Created at'},
                  {key: 'updatedAt', label: 'Updated at'},
                ]}
                scopedSlots = {{
                  id: (value) => {
                    return (
                      <td>
                        { value.id }
                      </td>
                    )
                  },
                  fromCoin: (value) => {
                    return (
                      <td>
                        {value.fromCoin.toUpperCase()}
                      </td>
                    )
                  },
                  toCoin: (value) => {
                    return (
                      <td>
                        {value.toCoin.toUpperCase()}
                      </td>
                    )
                  },
                  fromAmount: (value) => {
                    return (
                      <td>
                        { number(handleDecimal(value.fromCoin, value.fromAmount)) }
                      </td>
                    )
                  },
                  toAmount: (value) => {
                    return (
                      <td>
                        { number(handleDecimal(value.toCoin, value.toAmount)) }
                      </td>
                    )
                  },
                  handleStatus: (value) => {
                    return (
                      <td>
                        { handleStatus(value.handleStatus) }
                      </td>
                    )
                  },
                  createdAt: (value) => {
                    return (
                      <td>
                        { customDate(value.createdAt, 'YYYY-MM-DD HH:mm:ss') }
                      </td>
                    )
                  },
                  updatedAt: (value) => {
                    return (
                      <td>
                        { customDate(value.updatedAt, 'YYYY-MM-DD HH:mm:ss') }
                      </td>
                    )
                  },
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default RebalanceHistory
