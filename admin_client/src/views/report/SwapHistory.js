import { CDataTable } from "@coreui/react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CRow,
  CCol,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFormGroup,
  CInput,
  CFormText,
  CButton,
} from '@coreui/react'


import React, { useEffect, useState } from 'react'
import {useSelector} from "react-redux";

import {getReportSwapHistory, getCSVSwapHistory} from '../../actions/dmm';
import {
  dateToTimestamp,
  number,
  yyyymmdd,
  subDay,
  addDay,
  handleDecimal,
} from '../../utils/common';


const PAIR_LIST = ['ALL', 'DAI/KRW', 'USDT/KRW', 'USDC/KRW', 'WBTC/BTC', 'WETH/ETH']
const today = yyyymmdd(new Date())

const SwapHistory = () => {
  const list = useSelector(state => state.dmm.reportSwapHistory);

  const [start, setStart] = useState(subDay(7));
  const [end, setEnd] = useState(today);
  const [pair, setPair] = useState('ALL');

  const search = () => {
    getReportSwapHistory(dateToTimestamp(start), dateToTimestamp(addDay(end, 1)), getSwapPair() )
  }

  useEffect(() => {
    search();
  }, []);

  const resetOptions = () => {
    setStart(subDay(7))
    setEnd(today)
    setPair('ALL')
  }

  const getSwapPair = () => {
    if (pair.includes('/')) {
      return pair.replace(/\//g,'-').toLowerCase()
    }
  }

  const csvDownload = () => {
    getCSVSwapHistory(dateToTimestamp(start), dateToTimestamp(addDay(end, 1)), getSwapPair() )
  }

  const handleDate = (e) => {
    'start-date' === e.target.id ? setStart(e.target.value) : setEnd(e.target.value)
  }

  const handlePair = (e) => {
    setPair(e.target.innerText)
  }


  return (
    <>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              <h3>Swap History</h3>
              <hr/>

              <div>
                <CFormGroup row>
                  <CCol xs="12" md="2">
                    <CFormText className="help-block"><h6>Start Day</h6></CFormText>
                    <CInput
                      id="start-date"
                      type="date"
                      name="date-input"
                      value={start}
                      onChange={handleDate}
                    />
                  </CCol>
                  <CCol xs="12" md="2">
                    <CFormText className="help-block"><h6>End Day</h6></CFormText>
                    <CInput
                      id="end-date"
                      type="date"
                      name="date-input"
                      value={end}
                      onChange={handleDate}
                    />
                  </CCol>
                  <CCol xs="12" md="2" style={{maxWidth: '11%'}}>
                    <CFormText className="help-block"><h6>Pair</h6></CFormText>
                    <CDropdown className="m-1">
                      <CDropdownToggle style={{boxShadow: '0 1px 1px 0 rgb(60 75 100 / 14%), 0 2px 1px -1px rgb(60 75 100 / 12%), 0 1px 3px 0 rgb(60 75 100 / 20%)'}}>
                        {pair}
                      </CDropdownToggle>
                      <CDropdownMenu>
                        {
                          PAIR_LIST.map((val, idx) => {
                            return (
                              <CDropdownItem onClick={handlePair} key={val}>{val}</CDropdownItem>
                            )
                          })
                        }
                      </CDropdownMenu>
                    </CDropdown>
                  </CCol>
                  <CCol xs="12" md="1" style={{maxWidth: '8%', marginTop: '27px'}}>
                    <CButton onClick={resetOptions} variant="outline" color="secondary">초기화</CButton>
                  </CCol>
                  <CCol xs="12" md="1" style={{marginTop: '27px'}}>
                    <CButton onClick={search} variant="outline" color="warning">검색</CButton>
                  </CCol>
                  <CCol xs="12" md="1" style={{marginTop: '27px'}}>
                    <CButton onClick={csvDownload} variant="outline" color="success">CSV</CButton>
                  </CCol>
                </CFormGroup>
              </div>
            </CCardHeader>
            <CCardBody>
              <CDataTable
                // hover
                striped
                bordered
                pagination
                itemsPerPage={10}
                items={list}
                fields={[
                  {key: 'exSwapId', label: 'id'},
                  {key: 'fromName', label: 'From'},
                  {key: 'fromAmount', label: 'From Amount'},
                  {key: 'toName', label: 'To'},
                  {key: 'toAmount', label: 'To Amount'},
                  {key: 'price', label: 'Price'},
                  {key: 'swapVolume', label: 'Swap Volume'},
                  {key: 'expectedPnl', label: 'Expected PnL'},
                  {key: 'exTimeStr', label: 'time'},
                ]}
                scopedSlots = {{
                  exSwapId: (value) => {
                    return (
                      <td>
                        { value.exSwapId.substring(0,3) + '....' + value.exSwapId.substring(value.exSwapId.length-5, value.exSwapId.length) }
                      </td>
                    )
                  },
                  fromName: (value) => {
                    return (
                      <td>
                        {value.fromName.toUpperCase()}
                      </td>
                    )
                  },
                  toName: (value) => {
                    return (
                      <td>
                        {value.toName.toUpperCase()}
                      </td>
                    )
                  },
                  fromAmount: (value) => {
                    return (
                      <td>
                        { number(handleDecimal(value.fromName, value.fromAmount)) }
                      </td>
                    )
                  },
                  toAmount: (value) => {
                    return (
                      <td>
                        { number(handleDecimal(value.toName, value.toAmount)) }
                      </td>
                    )
                  },
                  price: (value) => {
                    return (
                      <td>
                        { number(parseInt(value.price)) }
                      </td>
                    )
                  },
                  swapVolume: (value) => {
                    return (
                      <td>
                        { number(parseInt(value.swapVolume)) }
                      </td>
                    )
                  },
                  expectedPnl: (value) => {
                    return (
                      <td>
                        { number(parseInt(value.expectedPnl)) }
                      </td>
                    )
                  },
                  exTimeStr: (value) => {
                    return (
                      <td>
                        {/*{unixToLocalTime(value.exTimeStr)}*/}
                        {value.exTimeStr}
                      </td>
                    )
                  },
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default SwapHistory
