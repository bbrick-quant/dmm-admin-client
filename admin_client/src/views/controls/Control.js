import {
  CRow,
} from '@coreui/react';

import React, {useEffect, useState} from 'react';

import QuoteCtrlComponent from '../../components/controls/QuoteCtrl';
import AutoRebalanceCtrlComponent from '../../components/controls/AutoRebalanceCtrl';
import ManualRebalanceCtrlComponent from '../../components/controls/ManualRebalanceCtrl';
import Spinner from '../../components/Spinner';
import {getQuoteList, getRebalanceList} from '../../actions/dmm';


const Control = () => {

  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    getQuoteList()
    getRebalanceList()
  }, []);

  return (
    <>
      <CRow>

        <Spinner
          isLoad={loaded}
        />

        <QuoteCtrlComponent
          setLoaded={setLoaded}
        />

        <ManualRebalanceCtrlComponent/>

        <AutoRebalanceCtrlComponent/>

      </CRow>
    </>
  )
}

export default Control
