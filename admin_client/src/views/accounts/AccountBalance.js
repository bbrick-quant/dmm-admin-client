import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CLabel,
  CFormGroup,
  CInputCheckbox,
} from '@coreui/react'

import { useSelector } from 'react-redux'
import {getFunds, getFund, clearFund, getFundList} from 'src/actions/fund'
import { getMarketPairs } from 'src/actions/marketInfo';
import { subscribeFixingRate, clearSubscribeFixingRate }  from 'src/websocket/orderbook';
import store from 'src/store';
import {SELECT_FUND} from "src/store/accountBalance";

import AccountBalanceComponent from 'src/components/accounts/AccountBalance';
import FixingRate from 'src/components/accounts/FixingRate';
import AccountPnL from "src/components/accounts/AccountPnL";
import AccountBalanceChart from "src/components/accounts/AccountBalanceChart";
import Transfer from "src/components/accounts/Transfer";
import InterestedAssetList from "src/components/accounts/InterestedAssetList";


const keyAssetList = ['BTC', 'ETH', 'KRW', 'USDT'];

const AccountBalance = () => {
  const assetSymbols = useSelector(state => state.accountBalance.filteredAssetSymbol);
  const fundList = useSelector(state => state.accountBalance.fundList);
  const selectedFund = useSelector(state => state.accountBalance.selectedFund);

  const [fieldsWithExchange, setFieldsWithExchange] = useState([]);
  const [balanceScopedSlots, setBalanceScopedSlots] = useState({});
  const [keyAsset, setKeyAsset] = useState(keyAssetList[0]);
  const [marketPairList, setMarketPairList] = useState([]);

  // Transfer
  const [showTransfer, setShowTransfer] = useState(false);
  const [transferAccountId, setTransferAccountId] = useState(0);
  const [transferAsset, setTransferAsset] = useState('');

  const openTransferModal = (accountId, asset) => {
    setTransferAccountId(accountId);
    setTransferAsset(asset);
    setShowTransfer(true);
  };

  const intl = new Intl.NumberFormat('ko-KR', { minimumFractionDigits: 10, maximumFractionDigits:10 })
  const numeralSlot = (key) => (value) => {
    const validAsset = value[key];
    if(validAsset && value.accountId && key !== 'KRW') {
      return (
        <td
          className="display-5 text-right"
          style={{cursor: 'pointer'}}
          onClick={()=>openTransferModal(value.accountId, key)}
        >
          <small>
            { intl.format(validAsset) }
          </small>
        </td>
      )
    }
    if(!value.accountId || key === 'KRW') {
      return (
        <td className="display-5 text-right">
          <small>
            {validAsset || validAsset === 0 ? intl.format(value[key]) : null}
          </small>
        </td>
      )
    }
    return (
      <td className="display-5 text-right">
      </td>
    )
  };

  const refreshData = () => {
    if(selectedFund.size === 0){
      clearFund();
    } else if(selectedFund.size === 1) {
      getFund([...selectedFund][0]);
    } else {
      getFunds();
    }
  };

  useEffect(()=>{
    refreshData();

    const interval = setInterval(refreshData, 60 * 1000);
    return () => {
      clearInterval(interval);
    }
  },[selectedFund])

  useEffect(()=>{
    clearFund();

    getFundList();
    getMarketPairs()
      .then((marketPairs)=>{
        setMarketPairList(marketPairs.map(val => ({
          marketId: val.market.id,
          exchange: val.market.exchange,
          pairId: val.id,
          pairInternalTicker: val.internalTicker,
        })));
      });
  }, []);

  useEffect(()=> {
    const fieldList = [];
    const slots = {};
    for(const key of assetSymbols) {
      fieldList.push({key, sorter: true, _classes: 'text-right'})
      slots[key] = numeralSlot(key);
    }
    setBalanceScopedSlots(slots);
    setFieldsWithExchange(['exchange', ...fieldList]);
  }, [assetSymbols]);

  const subscribeAsset = (symbol)=>{
    const getPairKey = (symbol, key) => {
      if (symbol === 'KRW') return 'BTC/KRW';
      if (symbol.indexOf('USD') !== -1) return 'BTC/USDT';
      return `${symbol}/${key}`;
    }

    if(symbol === 'BTC') {
      return;
    }

    const exchangeList = ['binance', 'bithumb', 'coinone'];
    const filteredPair = marketPairList
      .map(val => ({
        ...val,
        exchangeIdx: exchangeList.findIndex(ex => ex === val.exchange)
      }))
      .filter(val => val.exchangeIdx !== -1)
      .sort((a, b)=> a.exchangeIdx - b.exchangeIdx);

    for(const key of keyAssetList) {
      const pairKey = getPairKey(symbol, key);

      const pair = filteredPair.find(val => pairKey === val.pairInternalTicker);

      if(pair) {
        const inverse = symbol === 'KRW' || symbol.indexOf('USD') !== -1;
        if(inverse) {
          subscribeFixingRate(pair.marketId, pair.pairId, key, symbol);
        } else {
          subscribeFixingRate(pair.marketId, pair.pairId, symbol, key);
        }
        return;
      }
    }
  }

  useEffect(() => {
    for(const symbol of assetSymbols) {
      subscribeAsset(symbol);
    }

    return () => {
      clearSubscribeFixingRate();
    };
  }, [marketPairList, assetSymbols]);

  return (
    <>
      <CCard>
        <CCardHeader>
          <h4 className="card-title mb-0">펀드 목록</h4>
        </CCardHeader>
        <CCardBody>
          {
            fundList.map((val, idx)=>{
              const keyname = `fund-check-${idx}`;
              const checked = selectedFund.has(val.id);
              return (
                <CFormGroup
                  key={val.id}
                  variant="checkbox"
                  className="checkbox"
                  inline
                >
                  <CInputCheckbox
                    id={keyname}
                    name="checkbox2"
                    value={val.id}
                    checked={checked}
                    onChange={()=>{store.dispatch({type: SELECT_FUND, id: val.id, flag: !checked})}}
                  />
                  <CLabel
                    variant="checkbox"
                    className="form-check-label"
                    htmlFor={keyname}
                  >
                    {val.name}
                  </CLabel>
                </CFormGroup>
              )
            })
          }
        </CCardBody>
      </CCard>

      <InterestedAssetList/>

      <CRow>
        <CCol xl="9" lg="12">
          <AccountBalanceComponent
            keyAsset={keyAsset}
            fieldsWithExchange={fieldsWithExchange}
            balanceScopedSlots={balanceScopedSlots}
          />
          <FixingRate
            balanceScopedSlots={balanceScopedSlots}
            setKeyAsset={setKeyAsset}
            keyAsset={keyAsset}
          />
          <AccountPnL
            fieldsWithExchange={fieldsWithExchange}
            balanceScopedSlots={balanceScopedSlots}
            keyAsset={keyAsset}
          />
        </CCol>
        <CCol xl="3" lg="12">
          <AccountBalanceChart
            keyAsset={keyAsset}
          />
        </CCol>
      </CRow>
      <Transfer
        show={showTransfer}
        setModal={setShowTransfer}
        asset={transferAsset}
        accountId={transferAccountId}
      />
    </>
  )
}

export default AccountBalance
