import { UPDATE_FIXING_RATE, CLEAR_FIXING_RATE } from "../store/accountBalance";
import store from '../store';

let fixingRateConnection = {

};

const skipCount = 300;

export function subscribeFixingRate(exchange, asset, assetName, keyAsset) {
  try {
    const mapKey = `${exchange}_${asset}`;
    if(fixingRateConnection[mapKey]) fixingRateConnection[mapKey].close();

    const ws = new WebSocket(`ws://${window.location.host}/ws/v1/markets/${exchange}/orderbooks/${asset}/`);
    fixingRateConnection[mapKey] = ws;
    ws.updateDataCount = skipCount;

    ws.addEventListener('message', (event) => {
      if(!event?.data) return;

      ws.updateDataCount++;
      if(ws.updateDataCount > skipCount) {
        ws.updateDataCount = 0;
        const msg = JSON.parse(event?.data);
        const price = (msg.asks[0][0] + msg.bids[0][0]) /2;

        store.dispatch({type: UPDATE_FIXING_RATE, asset: assetName, key: keyAsset, price})
      }
    });

  } catch(e) {
    console.error(e);
  }
}

export function clearSubscribeFixingRate() {
  for(const mapKey of Object.keys(fixingRateConnection)) {
    fixingRateConnection[mapKey].close();
  }

  store.dispatch({type: CLEAR_FIXING_RATE})
  fixingRateConnection = {};
}
