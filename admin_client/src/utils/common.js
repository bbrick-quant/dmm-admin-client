
import moment from 'moment';
import NumberFormat from 'react-number-format';
import React from 'react';
import axios from '../axios';


export function unixToLocalTime(time) {
  return moment.unix(time).format('YYYY-MM-DD HH:mm:ss');
}

/**
 *  2021-07-11
 * */
export function getCurrentDate() {
  return moment().format('yyyy-MM-DD');
}

/**
 * param: Date
 * return: 2021-07-11
 * */
export function yyyymmdd(date) {
  return moment(date).format('yyyy-MM-DD');
}

export function customDate(date, format) {
  return moment(date).format(format);
}

export function addDay(date, n) {
  return moment(date).add(n, 'days').format('yyyy-MM-DD');
}

/**
 * param: 7
 * return: 일주일전 날짜
 * */
export function subDay(n) {
  return moment().subtract(n, 'days').format('yyyy-MM-DD');
}

export function subMonth(n) {
  return moment().subtract(n, 'month').format('yyyy-MM-DD');
}

export function dateToTimestamp(date) {
  return moment(date).format('X')
}

/**
 * 2021-07-01 ~ 2021-07-31
 * */
export function getThisMonth() {
  const startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
  const endOfMonth   = moment().endOf('month').format('YYYY-MM-DD');

  return `${startOfMonth}~${endOfMonth}`
}

/**
 * param: 2021-07
 * return: 2021-07-01 ~ 2021-07-31
 * */
export function getMonthStartEnd(date) {
  const startOfMonth = moment(date).startOf('month').format('YYYY-MM-DD');
  const endOfMonth   = moment(date).endOf('month').format('YYYY-MM-DD');

  return `${startOfMonth}~${endOfMonth}`
}


/**
 * number format
 * 10222323 => 10,222,323
 * */
export function number(val) {
  // return <NumberFormat value={val} decimalScale={3} displayType={'text'} thousandSeparator={true} />
  return <NumberFormat value={val} displayType={'text'} thousandSeparator={true} />
}

export function handleDecimal(coin, amount) {
  switch (coin) {
    case 'btc':
      return Number(amount).toFixed(5);
    case 'wbtc':
      return Number(amount).toFixed(5);
    case 'krw':
      return parseInt(amount);
    default:
      return Number(amount).toFixed(3);
  }
}

export function getUsername() {
  return localStorage.getItem('username');
}

/**
 * dai-krw => DAI/KRW
 * */
export function viewPairFormat(pair) {
  return pair.replace(/-/g,'/').toUpperCase()
}

export function csvDownload(url) {
  axios({
    method: 'GET',
    url: url,
    responseType: 'blob'
  }).then(response => {
    if (response.data) {
      let filename = '';
      const disposition = response.headers['content-disposition'];
      const filenameRegex = /filename[^;=\n]*=(UTF-8(['"]).*?\2|[^;\n]*)/;
      const matches = filenameRegex.exec(disposition);
      if (matches != null && matches[1]) {
        filename = matches[1].replace(/['"]/g, '');
      }

      const url = window.URL.createObjectURL(new Blob([response.data], { type: response.headers['content-type'] }));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', decodeURI(filename));
      document.body.appendChild(link);
      link.click();
    }
  })
}
