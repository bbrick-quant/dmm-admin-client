

export function getFundInfo(funds) {
  const exchangeSymbolSet = new Set();
  const assetSet = new Set();
  const accounts = [];
  const initialSet = {};
  const debtSet = {};
  const total = {};

  if(!funds || !funds.length) {
    return {
      assetSymbols: [],
      exchangeSymbols: [],
      accounts,
      initialSet,
      debtSet,
      total,
    };
  }

  for(const [,fund] of funds.entries()) {
    fund.accounts.forEach(val=> exchangeSymbolSet.add(val.name));
    for(const [key, value] of Object.entries(fund.initialBalance)) {
      if(!initialSet[key]) initialSet[key] = 0;
      initialSet[key] += value;

      assetSet.add(key);
    }

    for(const [key, value] of Object.entries(fund?.debtBalance || {})) {
      if(!debtSet[key]) debtSet[key] = 0;
      debtSet[key] += value;

      assetSet.add(key);
    }


    for(const [, account] of fund.accounts.entries()) {
      const data = {
        exchange: account.name,
        accountId: account.id,
      }
      for(const apiKey of account.apiKeys) {
        const { balanceSnapshot } = apiKey;

        if(!balanceSnapshot?.assets) continue;

        for(const assetKey in balanceSnapshot.assets) {
          const size = (balanceSnapshot.assets[assetKey].available || 0) + (balanceSnapshot.assets[assetKey].locked || 0);
          if(size > 0) {
            assetSet.add(assetKey);
            data[assetKey] = (data?.[assetKey] || 0) + size;
          }

          total[assetKey] = (total?.[assetKey] || 0) + size;
        }
      }
      accounts.push(data);
    }
  }

  for(const [asset] of assetSet.entries()) {
    if(!initialSet[asset]) initialSet[asset]=0;
    if(!debtSet[asset]) debtSet[asset]=0;
    if(!total[asset]) total[asset] = 0;
  }

  return {
    assetSymbols: [...assetSet],
    exchangeSymbols: [...exchangeSymbolSet],
    accounts,
    initialSet,
    debtSet,
    total,
  }
};
