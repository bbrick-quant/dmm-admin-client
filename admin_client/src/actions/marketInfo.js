import axios from '../axios';
import store from '../store';

import { UPDATE_MARK_PAIR_LIST } from '../store/marketInfo';

export async function getMarketPairs() {
  const res = await axios.get('/api/v1/markets/pairs/');

  const data = res?.data || [];
  store.dispatch({type: UPDATE_MARK_PAIR_LIST, marketPairs: data})
  return data;
}
