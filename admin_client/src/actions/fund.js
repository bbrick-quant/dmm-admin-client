import axios from '../axios';
import store from '../store';

import { UPDATE_FUND_LIST, UPDATE_FUND, CLEAR_FUND } from "../store/accountBalance";
import { SET_TRANSFER_HISTORIES } from "../store/transfer";

export async function getFundList() {
  const res = await axios.get('/api/v1/funds/');

  const data = res?.data || [];
  store.dispatch({type: UPDATE_FUND_LIST, funds: data})
}

export async function getFunds() {
  const res = await axios.get('/api/v1/funds/');

  const data = res?.data || [];
  store.dispatch({type: UPDATE_FUND, funds: data})
}

export async function getFund(id) {
  const res = await axios.get(`/api/v1/funds/${id}/`);

  const data = res?.data;
  if(data) {
    store.dispatch({type: UPDATE_FUND, funds: [data]})
  }
}

export async function getFundSnapshot(id, date) {
  let dateQuery = '';
  if(date) {
    dateQuery = `?date=${date}`
  }
  const res = await axios.get(`/api/v1/funds/${id}/balance-snapshots/${dateQuery}`);
  const data = res?.data;
  if(data) {
    store.dispatch({type: UPDATE_FUND, funds: [data]})
  }
}

export async function getTransferHistory() {
  const res = await axios.get('/api/v1/transfers/');
  const data = res?.data || [];

  store.dispatch({type: SET_TRANSFER_HISTORIES,histories: data});
}

function sleep(ms) {
  return new Promise((r) => setTimeout(r, ms));
}

export async function postTransfer({
  size,
  currency,
  description,
  from_account,
  to_account,
}) {
  const res = await axios.post(`/api/v1/transfers/`, {
    size,
    currency,
    description,
    from_account,
    to_account,
  });
  return res;
}

export function clearFund() {
  store.dispatch({type: CLEAR_FUND})
}
