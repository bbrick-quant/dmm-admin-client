import axios from '../axios';
import store from '../store';

import {
  UPDATE_SWAP_HISTORY,
  UPDATE_BALANCE,
  UPDATE_VOLUME,
  REPORT_SWAP_HISTORY,
  UPDATE_VOLUME_CHART,
  UPDATE_QUOTE_LIST,
  UPDATE_REBALANCE_LIST,
  UPDATE_DASHBOARD_REBALANCE_HISTORY,
  REPORT_REBALANCE_HISTORY,
} from '../store/dmm';
import {addDay, getCurrentDate, subMonth, csvDownload} from '../utils/common';

export async function getBalance() {
  const res = await axios.get('/api/v1/dmm/balance/');

  const data = res?.data || [];
  store.dispatch({type: UPDATE_BALANCE, balance: data})
}

export async function getVolume(date) {
  let dateQuery = '';
  if (date) {
    let tmpDate = date.split('~')
    dateQuery = `?start=${tmpDate[0]}&end=${tmpDate[1]}`
  }
  const res = await axios.get(`/api/v1/dmm/volume/${dateQuery}`);

  const data = res?.data || [];
  store.dispatch({type: UPDATE_VOLUME, volume: data})
}

export async function getSwapHistory() {
  const res = await axios.get('/api/v1/dmm/swap_history/');

  const data = res?.data || [];
  store.dispatch({type: UPDATE_SWAP_HISTORY, swapHistoryList: data})
}

export async function getDashboardRebalanceHistory() {
  const res = await axios.get('/api/v1/dmm/rebalance_history/');

  const data = res?.data || [];
  store.dispatch({type: UPDATE_DASHBOARD_REBALANCE_HISTORY, dashboardRebalanceHistoryList: data})
}

export async function getReportSwapHistory(start, end, pair) {
  let param = `?start=${start}&end=${end}`

  if ('undefined' !== typeof pair) {
    param += `&pair=${pair}`
  }

  const res = await axios.get(`/api/v1/dmm/report/swap_history/${param}`);

  const data = res?.data || [];
  store.dispatch({type: REPORT_SWAP_HISTORY, reportSwapHistory: data})
}

export async function getReportRebalanceHistory(start, end) {
  let param = `?start=${start}&end=${end}`

  const res = await axios.get(`/api/v1/dmm/report/rebalance_history/${param}`);

  const data = res?.data || [];
  store.dispatch({type: REPORT_REBALANCE_HISTORY, rebalanceHistory: data})
}

export async function getCSVSwapHistory(start, end, pair) {
  let param = `?start=${start}&end=${end}`

  if ('undefined' !== typeof pair) {
    param += `&pair=${pair}`
  }

  csvDownload(`/api/v1/dmm/report/swap_history/csv/${param}`)
}

export async function getCSVRebalanceHistory(start, end) {
  let param = `?start=${start}&end=${end}`

  csvDownload(`/api/v1/dmm/csv/rebalance_history/${param}`)
}

export async function getVolumeChart() {
  let param = `?start=${subMonth(1)}&end=${addDay(getCurrentDate(), 1)}`

  const res = await axios.get(`/api/v1/dmm/chart/volume/${param}`);

  const data = res?.data || [];
  store.dispatch({type: UPDATE_VOLUME_CHART, volumeChart: data})
}

export async function getQuoteList() {
  const res = await axios.get(`/api/v1/dmm/control/quote/`);

  const data = res?.data || [];
  store.dispatch({type: UPDATE_QUOTE_LIST, quoteList: data})
}

export async function getRebalanceList() {
  const res = await axios.get(`/api/v1/dmm/control/rebalance_list/`);

  const data = res?.data || [];
  store.dispatch({type: UPDATE_REBALANCE_LIST, rebalanceList: data})
}

export async function getReqQouteDetail(id) {
  const res = await axios.get(`/api/v1/dmm/control/quote/${id}/`);

  return res;
}
