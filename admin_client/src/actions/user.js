import axios from '../axios'

export async function Login(username, password) {
  const res = await axios.post('/api/v1/users/token/', {username, password});

  const token = res.data.token;
  if(res?.data?.token) {
    localStorage.setItem('tt', token); // tt: token
    localStorage.setItem('username', username);

    axios.defaults.headers.common = `Token ${token}`;
    window.location.href = "/";
  }
}

export async function Logout() {
  localStorage.removeItem('tt'); // tt: token
  localStorage.removeItem('username');

  window.location.href = '/login';
  await axios.post('/api/v1/users/logout'); // todo: 서버접속안될때 어떻게 되나 확인 필요
}
