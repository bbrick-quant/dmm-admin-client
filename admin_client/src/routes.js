import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Control = React.lazy(() => import('./views/controls/Control'));
const ReportSwapHistory = React.lazy(() => import('./views/report/SwapHistory'));
const ReportRebalanceHistory = React.lazy(() => import('./views/report/RebalanceHistory'));

const AccountBalance = React.lazy(() => import('./views/accounts/AccountBalance'));


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/control', name: 'Report', component: Control },
  { path: '/accountBalance', name: 'AccountBalance', component: AccountBalance },
  { path: '/report/swap', name: 'Report', component: ReportSwapHistory },
  { path: '/report/rebalance', name: 'Report', component: ReportRebalanceHistory },
];

export default routes;
