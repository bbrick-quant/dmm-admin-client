const initialState = {
  histories: [],
}

export const SET_TRANSFER_HISTORIES = 'transfer/SET_TRANSFER_HISTORIES'

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case SET_TRANSFER_HISTORIES: {
      let { histories } = rest;

      histories = histories.map(val => ({
        ...val,
        currency: val.currency.toUpperCase(),
      }));
      histories.sort((a, b) => {
        if(a.createdAt > b.createdAt) return -1;
        if(a.createdAt < b.createdAt) return 1;
        return 0;
      })

      return {
        ...state,
        histories,
      }
    }
    default:
      return state;
  }
}

export default changeState;
