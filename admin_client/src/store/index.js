import { createStore, combineReducers } from 'redux';

import sidebarReducer from './sidebar';
import dmm from './dmm';
import accountBalanceReducer from './accountBalance';
import transfer from './transfer';

const rootReducer = combineReducers({
  // Define a top-level state field named `todos`, handled by `todosReducer`
  sidebar: sidebarReducer,
  dmm,
  accountBalance: accountBalanceReducer,
  transfer,
})
const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
export default store
