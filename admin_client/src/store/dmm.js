
const initialState = {
  swapHistoryList: [],
  dashboardRebalanceHistoryList: [],
  balance: [],
  volume: {},
  reportSwapHistory: [],
  volumeChart: [],
  quoteList: [],
  rebalanceList: [],
  rebalanceHistoryList: [],
}

export const UPDATE_BALANCE = 'dmm/UPDATE_BALANCE';
export const UPDATE_VOLUME = 'dmm/UPDATE_VOLUME';
export const UPDATE_SWAP_HISTORY = 'dmm/UPDATE_SWAP_HISTORY';
export const UPDATE_DASHBOARD_REBALANCE_HISTORY = 'dmm/UPDATE_DASHBOARD_REBALANCE_HISTORY';
export const UPDATE_VOLUME_CHART = 'dmm/UPDATE_VOLUME_CHART';
export const UPDATE_QUOTE_LIST = 'dmm/UPDATE_QUOTE_LIST';
export const UPDATE_REBALANCE_LIST = 'dmm/UPDATE_REBALANCE_LIST';

export const REPORT_SWAP_HISTORY = 'dmm/REPORT_SWAP_HISTORY';
export const REPORT_REBALANCE_HISTORY = 'dmm/REPORT_REBALANCE_HISTORY';

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case UPDATE_BALANCE: {
      let {balance} = rest;

      return {
        ...state,
        balance: balance,
      }
    }
    case UPDATE_VOLUME: {
      let {volume} = rest;

      return {
        ...state,
        volume: volume,
      }
    }
    case UPDATE_SWAP_HISTORY: {
      let {swapHistoryList} = rest;

      return {
        ...state,
        swapHistoryList: swapHistoryList,
      }
    }
    case UPDATE_DASHBOARD_REBALANCE_HISTORY: {
      let {dashboardRebalanceHistoryList} = rest;

      return {
        ...state,
        dashboardRebalanceHistoryList: dashboardRebalanceHistoryList,
      }
    }
    case REPORT_SWAP_HISTORY: {
      let {reportSwapHistory} = rest;

      return {
        ...state,
        reportSwapHistory: reportSwapHistory,
      }
    }
    case UPDATE_VOLUME_CHART: {
      let {volumeChart} = rest;

      return {
        ...state,
        volumeChart: volumeChart,
      }
    }
    case UPDATE_QUOTE_LIST: {
      let {quoteList} = rest;

      return {
        ...state,
        quoteList: quoteList,
      }
    }
    case UPDATE_REBALANCE_LIST: {
      let {rebalanceList} = rest;

      return {
        ...state,
        rebalanceList: rebalanceList,
      }
    }
    case REPORT_REBALANCE_HISTORY: {
      let {rebalanceHistory} = rest;

      return {
        ...state,
        rebalanceHistory: rebalanceHistory,
      }
    }

    default:
      return state;
  }
}

export default changeState;
