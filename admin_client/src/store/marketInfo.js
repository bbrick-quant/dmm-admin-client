const initialState = {
  marketPairs: [],
}

export const UPDATE_MARK_PAIR_LIST = 'marketInfo/UPDATE_MARK_PAIR_LIST';

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case UPDATE_MARK_PAIR_LIST:
      const { marketPairs } = rest;

      return {
        ...state,
        marketPairs,
      }
    default:
      return state
  }
}

export default changeState;
