import { getFundInfo } from "../utils/converter";

const initialState = {
  assetSymbols: [/*'BTC', 'ETH', 'ATOM', 'LUNA', 'KRW', 'USD'*/],
  interestedAssets: new Set(JSON.parse( window.localStorage.getItem('InterestedAssetList') || "[]" )),
  filteredAssetSymbol: [],

  exchangeSymbols: [/*'Bithumb', 'Huobi'*/],
  accounts: [],
  originFixingRate: {
  },
  calcFixingRate: {

  },
  debtSet: {},
  initialSet: {},

  selectedFund: new Set(),
  fundList: [],
  apiKeyList: [],
}

export const UPDATE_FUND_LIST = 'account_balance/UPDATE_FUND_LIST';
export const UPDATE_FUND = 'account_balance/UPDATE_FUND';
export const CLEAR_FUND = 'account_balance/CLEAR_FUND';
export const SELECT_FUND = 'account_balance/SELECT_FUND';
export const SET_SELECT_FUND = 'account_balance/SET_SELECT_FUND';
export const SELECT_INTERESTED_ASSET = 'account_balance/SELECT_INTERESTED_ASSET';

export const UPDATE_FIXING_RATE = 'account_balance/UPDATE_FIXING_RATE';
export const CLEAR_FIXING_RATE = 'account_balance/CLEAR_FIXING_RATE';

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case UPDATE_FUND_LIST: {
      let {funds} = rest;

      const username = localStorage.getItem('username');
      if(username === 'peertec') {
        funds = funds.filter(val => val.id === 4)
      }

      let selectedFund = state.selectedFund;

      let fundList = state.fundList;

      const newFundList = funds.map(val => ({
        id: val.id,
        name: val.name,
      }));

      if (JSON.stringify(fundList) !== JSON.stringify(newFundList)) {
        fundList = newFundList;
        if(selectedFund.size === 0 && fundList.length !== 0) {
          const fund = fundList.find(val => val.name === 'PeerTec Arbitrage Fund');
          if(fund){
            selectedFund = new Set(state.selectedFund);
            selectedFund.add(fund.id)
          }
        }
        if(selectedFund.size === 0 && fundList.length === 1) {
          const fund = fundList[0];
          if(fund){
            selectedFund = new Set(state.selectedFund);
            selectedFund.add(fund.id)
          }
        }
      }

      let apiKeyList = state.apiKeyList;
      const newApiKeyList = [];
      for(const fund of funds) {
        for(const account of fund.accounts) {
          newApiKeyList.push({
            id: account.id,
            fundName: fund.name,
            accountName: account.name,
          })
        }
      }
      if(JSON.stringify(apiKeyList) !== JSON.stringify(newApiKeyList)) {
        apiKeyList = newApiKeyList;
      }

      return {
        ...state,
        apiKeyList,
        fundList,
        selectedFund,
      }
    }
    case UPDATE_FUND: {
      const {funds} = rest;
      const fundInfo = getFundInfo(funds);

      if(JSON.stringify(fundInfo.assetSymbols) === JSON.stringify(state.assetSymbols)) {
        delete fundInfo.assetSymbols;
      }

      if(JSON.stringify(fundInfo.exchangeSymbols) === JSON.stringify(state.exchangeSymbols)) {
        delete fundInfo.exchangeSymbols;
      }

      const assetSymbol = fundInfo.assetSymbols || state.assetSymbols;
      let filteredAssetSymbol = state.filteredAssetSymbol;
      const newFilter = assetSymbol.filter(val => state.interestedAssets.has(val))

      if(JSON.stringify(filteredAssetSymbol) !== JSON.stringify(newFilter)) {
        filteredAssetSymbol = newFilter;
      }


      return {
        ...state,
        ...{
          ...fundInfo,
        },
        filteredAssetSymbol,
      }
    }
    case CLEAR_FUND: {
      let selectedFund = state.selectedFund;
      if(state.selectedFund.size > 0) {
        selectedFund = new Set();
      }
      return {
        ...state,

        assetSymbols: [],
        filteredAssetSymbol: [],
        exchangeSymbols: [],
        accounts: [],

        debtSet: {},
        initialSet: {},
      }
    }
    case UPDATE_FIXING_RATE: {
      const { asset, key, price } = rest;
      const originFixingRate = {
        ...state.originFixingRate,
      }
      const { assetSymbols } = state;

      if(!originFixingRate[asset]) originFixingRate[asset] = {};
      originFixingRate[asset][key] = price;

      const calcFixingRate = {};
      for(const asset of assetSymbols) {
        calcFixingRate[asset] = {
          ...originFixingRate[asset]
        };
        calcFixingRate[asset][asset] = 1;
      }

      const keyAssetList = ['BTC', 'ETH', 'KRW', 'USDT'];
      const calcRate = (currency, base) => {
        if (calcFixingRate?.[base]?.[currency]) {
          return 1 / calcFixingRate[base][currency];
        }

        for (let basebase of keyAssetList) {
          if (base === basebase) continue;
          if (calcFixingRate?.[base]?.[basebase] && calcFixingRate?.[currency]?.[basebase])
            return  calcFixingRate[currency][basebase] / calcFixingRate[base][basebase];
        }

        return undefined;
      }

      for (let base of keyAssetList) {
        for (let currency of assetSymbols) {
          if (calcFixingRate[currency][base]) continue;

          calcFixingRate[currency][base] = calcRate(currency, base);
        }
      }

      return {
        ...state,
        originFixingRate,
        calcFixingRate,
      }
    }
    case CLEAR_FIXING_RATE: {
      return {
        ...state,
        originFixingRate: {},
        calcFixingRate: {},
      }
    }
    case SELECT_INTERESTED_ASSET: {
      const {asset, flag} = rest;
      const interestedAssets = new Set(state.interestedAssets);

      if(flag) { interestedAssets.add(asset); }
      else { interestedAssets.delete(asset); }

      window.localStorage.setItem('InterestedAssetList', JSON.stringify([...interestedAssets]));

      const filteredAssetSymbol = state.assetSymbols.filter(val => interestedAssets.has(val));

      return {
        ...state,
        interestedAssets,
        filteredAssetSymbol,
      }
    }
    case SELECT_FUND: {
      const {id, flag} = rest;

      const selectedFund = new Set(state.selectedFund);
      if(flag) { selectedFund.add(id); }
      else { selectedFund.delete(id); }

      return {
        ...state,
        selectedFund,
      }
    }
    case SET_SELECT_FUND: {
      const {id} = rest;

      const selectedFund = new Set();
      selectedFund.add(id);

      return {
        ...state,
        selectedFund,
      }
    }
    default:
      return state;
  }
}

export default changeState;
