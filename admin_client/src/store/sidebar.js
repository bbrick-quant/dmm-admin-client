
const initialState = {
  sidebarShow: 'responsive'
}

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'sidebar/set':
      return {...state, ...rest }
    default:
      return state
  }
}

export default changeState;