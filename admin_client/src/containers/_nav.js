import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Control',
    to: '/control',
    icon: 'cil-code',
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Report',
    route: '/buttons',
    icon: 'cil-cursor',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Swap History',
        to: '/report/swap',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Rebalance History',
        to: '/report/rebalance',
      },
      /*{
        _tag: 'CSidebarNavItem',
        name: 'Brand buttons',
        to: '/buttons/brand-buttons',
      },*/
    ],
  },
  /*
  {
    _tag: 'CSidebarNavItem',
    name: 'Account Balance',
    to: '/accountBalance',
    icon: 'cil-code',
  },
  */
]

export default _nav
