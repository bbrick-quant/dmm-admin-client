import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'


// sidebar nav config
import navigation from './_nav'

const TheSidebar = () => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebar.sidebarShow)

  const [filteredNav, setFilteredNav] = useState([]);

  useEffect(()=>{
    const username = localStorage.getItem('username');
    const peertecNav = ['Strategy Control', 'Account Balance'];
    if(username === 'peertec') {
      setFilteredNav(navigation.filter(val => peertecNav.includes(val.name)));
      return;
    }

    setFilteredNav(navigation);
  }, []);

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({type: 'sidebar/set', sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        {/*<CIcon
          className="c-sidebar-brand-full"
          name="logo-negative"
          height={35}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          height={35}
        />*/}
        <h3>DMM BackOffice</h3>
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={filteredNav}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
