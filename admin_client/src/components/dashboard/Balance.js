import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
} from '@coreui/react';

import React, { useState, useEffect } from "react";
import {useSelector} from "react-redux";

import BalanceChart from "./BalanceChart";
import CountUp from 'react-countup';

const dmmCurrencyList = ['krw', 'dai', 'usdc', 'usdt', 'btc', 'eth', 'weth', 'wbtc']


const Balance = () => {
  const balanceList = useSelector(state => state.dmm.balance);

  const [currencyList, setCurrencyList] = useState([]);
  const [chartData, setChartData] = useState([]);

  useEffect(() => {
    const list = [];
    const dmmListForChart = [];
    const chartDataList = [
      { data: [] },
      { data: [] },
      { data: [] },
    ]

    for(let i=0, max=balanceList.length; i < max; i+=1) {
      dmmCurrencyList.forEach((val, idx) => {
        if (balanceList[i].coin === val) {
          list.push(balanceList[i])
          dmmListForChart.push({
            // id: balanceList[i].id,
            name: balanceList[i].coin.toUpperCase(),
            y: Number(balanceList[i].balance),
          })
        }
      })
    }

    list.sort((a, b) => b.balance - a.balance);
    dmmListForChart.sort((a, b) => b.y - a.y);

    for(let i=0, max=dmmListForChart.length; i < max; i+=1) {
      if ('KRW' === dmmListForChart[i].name || 'DAI' === dmmListForChart[i].name || 'USDC' === dmmListForChart[i].name || 'USDT' === dmmListForChart[i].name) {
        // TODO: DAI, USDC, USDT 정확한 시세 처리 필요
        if ('KRW' !== dmmListForChart[i].name) {
          dmmListForChart[i].y = dmmListForChart[i].y * 1100;
        }
        chartDataList[0]['data'].push(dmmListForChart[i])
      }
      else if ('ETH' === dmmListForChart[i].name || 'WETH' === dmmListForChart[i].name) {
        chartDataList[1]['data'].push(dmmListForChart[i])
      }
      else if ('BTC' === dmmListForChart[i].name || 'WBTC' === dmmListForChart[i].name) {
        chartDataList[2]['data'].push(dmmListForChart[i])
      }
    }

    setChartData(chartDataList)
    setCurrencyList(list)

  }, [balanceList]);

  const handleCountupDecimal = (item) => {
    switch (item.coin) {
      case 'btc':
        return 5;
      case 'wbtc':
        return 5;
      case 'krw':
        return 0;
      default:
        return 3;
    }
  }

  return (
    <CCard>
      <CCardHeader>
        <h3>Balance</h3>
      </CCardHeader>
      <CCardBody>
        <CRow>
          {
            chartData.map((item, i) => {
              return (
                <BalanceChart key={i} data={item}/>
              )
            })
          }
        </CRow>
        <CRow>
          {
            currencyList.map((val, idx) => {
              return (
                <CCol key={idx} xl="2" md="4" sm="6" xs="12">
                  <h4>
                    <CountUp
                      end={val.balance}
                      delay={0}
                      decimals={handleCountupDecimal(val)}
                      duration={1}
                      separator={','}
                      preserveValue={false}
                    >
                      {({ countUpRef }) => (
                        <span
                          ref={countUpRef}
                        />
                      )}
                    </CountUp> { val.coin.toUpperCase() }
                  </h4>
                </CCol>
              )
            })
          }
        </CRow>
      </CCardBody>
    </CCard>
  )
}

export default Balance;
