import React from 'react';

import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const pieColors = (function () {
  // docs: https://api.highcharts.com/highcharts/plotOptions.pie.colors
  return [
    '#0081CF',
    '#FFC75F',
    '#6380F9',
    '#FF6F91',
    '#FF9671',
    '#F9F871',
    '#845EC2',
    '#008E9B',
    '#D65DB1',
  ]
}());


const useOptions = series => {
  return {
    chart: {
      backgroundColor: 'transparent',
      plotBackgroundColor: null,
      plotShadow: false,
      animation: false,
      type: 'pie',
      width: 300,
      height: 200,
      style: {
        'float': 'left',
        'top': '-20px',
      }
    },
    credits: {
      enabled: false
    },
    title: {
      text: null
    },
    plotOptions: {
      series: {
        states: {
          inactive: {
            opacity: 1
          }
        }
      },
      pie: {
        colors: pieColors,
        borderColor: 'transparent',
        animation: false,
        dataLabels:{
          distance: -40,
          style:{
            fontSize: '18px',
            fontWeight: 'bold',
            // color: '#000',
            color: '#fff',
            textOutline: 'none'
          }
        },
        // showInLegend: true,
        point: {
          events: {
            legendItemClick: function() {
              return false;
            }
          }
        }
      }
    },
    series
  };
};


export default function BalanceChart({ data }) {
  const options = useOptions(data);

  return (
    <div>
      <HighchartsReact highcharts={Highcharts} options={options} />
    </div>
  );
}
