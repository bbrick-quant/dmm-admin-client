import {CCard, CCardBody, CCardHeader, CCol, CDataTable} from "@coreui/react";
import React, { useEffect } from "react";
import {useSelector} from "react-redux";

import { unixToLocalTime, number, viewNumber, handleDecimal } from "../../utils/common";

const SwapHistory = () => {

  const swapHistoryList = useSelector(state => state.dmm.swapHistoryList);

  useEffect(() => {

  }, []);


  return (
    <CCol lg={12} xl={6}>
      <CCard>
        <CCardHeader>
          <h4>Swap History</h4>
        </CCardHeader>
        <CCardBody>
          <CDataTable
            items={swapHistoryList}
            fields={[
              {key: 'fromName', label: 'From'},
              {key: 'fromAmount', label: 'From Amount'},
              {key: 'toName', label: 'To'},
              {key: 'toAmount', label: 'To Amount'},
              {key: 'expectedPnl', label: 'Expected PnL'},
              {key: 'exTimeStr', label: 'time'},
            ]}
            scopedSlots = {{
              fromName: (value) => {
                return (
                  <td>
                    {value.fromName.toUpperCase()}
                  </td>
                )
              },
              toName: (value) => {
                return (
                  <td>
                    {value.toName.toUpperCase()}
                  </td>
                )
              },
              fromAmount: (value) => {
                return (
                  <td>
                    { number(handleDecimal(value.fromName, value.fromAmount)) }
                  </td>
                )
              },
              toAmount: (value) => {
                return (
                  <td>
                    { number(handleDecimal(value.toName, value.toAmount)) }
                  </td>
                )
              },
              expectedPnl: (value) => {
                return (
                  <td>
                    { number(parseInt(value.expectedPnl)) }
                  </td>
                )
              },
              exTimeStr: (value) => {
                return (
                  <td>
                    {unixToLocalTime(value.exTimeStr)}
                  </td>
                )
              },
            }}
          />
        </CCardBody>
      </CCard>
    </CCol>
  )
}

export default SwapHistory;
