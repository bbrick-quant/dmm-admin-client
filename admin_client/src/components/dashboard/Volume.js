import {
  CCallout,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CFormGroup,
  CSelect,
  CLabel,
} from '@coreui/react';

import React, { useEffect, useState } from "react";
import {useSelector} from "react-redux";

import {number, getMonthStartEnd, getCurrentDate, getThisMonth} from '../../utils/common';
import CountUp from 'react-countup';
import axios from '../../axios';

const today = getCurrentDate();
const thisMonth = getThisMonth();
const ALL_MONTH = ['Month','01','02','03','04','05','06','07','08','09','10','11','12']


const Volume = () => {
  const volumeList = useSelector(state => state.dmm.volume);

  const [isSearch, setIsSearch] = useState(false);
  // TODO: 년도 처리
  const [year, setYear] = useState('2021');
  const [month, setMonth] = useState('');
  const [monthVolume, setMonthVolume] = useState({});

  useEffect(() => {
    if (month && 'Month' !== month) {
      const date = getMonthStartEnd(year+'-'+month);
      getMonthVolume(date);
      setIsSearch(true);
      return;
    }
    setIsSearch(false);
  }, [month]);

  async function getMonthVolume(date) {
    let tmpDate = date.split('~')
    const dateQuery = `?start=${tmpDate[0]}&end=${tmpDate[1]}`

    const res = await axios.get(`/api/v1/dmm/volume/${dateQuery}`);
    const data = res?.data || {};
    setMonthVolume(data);
  }

  const MonthlyData = () => {
    return(
      isSearch &&
      <CRow>
        <CCol sm="6">
          <CCallout color="info">
            <strong className="text-muted">
              {year}년 {month}월 Volume
            </strong>
            <br />
            <strong className="h4">{ 0 < monthVolume.monthVolume ? number(parseInt(monthVolume.monthVolume)) : 0 } KRW</strong>
          </CCallout>
        </CCol>
        <CCol sm="6">
          <CCallout color="info">
            <strong className="text-muted">
              {year}년 {month}월 Profit
            </strong>
            <br />
            <strong className="h4">{ 0 < monthVolume.monthProfit ? number(parseInt(monthVolume.monthProfit)) : 0 } KRW</strong>
          </CCallout>
        </CCol>
      </CRow>
    )
  }

  return (
    <CRow>
      <CCol lg={12} xl={6}>
        <CCard>
          <CCardHeader>
            <CFormGroup row style={{marginBottom: '-8px'}}>
              <CCol md="7">
                <CLabel>
                  <h4>Monthly: {thisMonth}</h4>
                </CLabel>
              </CCol>
              <CCol xs="12" md="2">
                <CSelect
                  custom style={{width: '100px'}}
                  value={year}
                  onChange={e => {setYear(e.target.value)}}
                >
                  <option value="0">{year}</option>
                </CSelect>
              </CCol>
              <CCol xs="12" md="2">
                <CSelect
                  custom style={{width: '100px'}}
                  value={month}
                  onChange={e => {setMonth(e.target.value)}}
                >
                  {
                    ALL_MONTH.map((month, idx)=>(
                      <option key={idx}>{month}</option>
                    ))
                  }
                </CSelect>
              </CCol>

            </CFormGroup>
          </CCardHeader>
          <CCardBody>
            <CRow>
              <CCol sm="6">
                <CCallout color="info">
                  <strong className="text-muted">Volume</strong>
                  <br />
                  <strong className="h4">
                    <CountUp
                      end={0 < volumeList.monthVolume ? parseInt(volumeList.monthVolume) : 0}
                      delay={0}
                      decimals={0}
                      duration={1}
                      separator={','}
                      preserveValue={true}
                    >
                      {({ countUpRef }) => (
                        <span
                          ref={countUpRef}
                        />
                      )}
                    </CountUp> KRW
                  </strong>
                </CCallout>
              </CCol>
              <CCol sm="6">
                <CCallout color="info">
                  <strong className="text-muted">Profit</strong>
                  <br />
                  <strong className="h4">
                    <CountUp
                      end={0 < volumeList.monthProfit ? parseInt(volumeList.monthProfit) : 0}
                      delay={0}
                      decimals={0}
                      duration={1}
                      separator={','}
                      preserveValue={true}
                    >
                      {({ countUpRef }) => (
                        <span
                          ref={countUpRef}
                        />
                      )}
                    </CountUp> KRW
                  </strong>
                </CCallout>
              </CCol>
            </CRow>

            <MonthlyData/>

          </CCardBody>
        </CCard>
      </CCol>

      <CCol lg={12} xl={6}>
        <CCard>
          <CCardHeader>
            <h4>Today: {today}</h4>
          </CCardHeader>
          <CCardBody>
            <CRow>
              <CCol sm="6">
                <CCallout color="info">
                  <strong className="text-muted">Volume</strong>
                  <br />
                  <strong className="h4">
                    <CountUp
                      end={0 < volumeList.todayVolume ? parseInt(volumeList.todayVolume) : 0}
                      delay={0}
                      decimals={0}
                      duration={1}
                      separator={','}
                      preserveValue={true}
                    >
                      {({ countUpRef }) => (
                        <span
                          ref={countUpRef}
                        />
                      )}
                    </CountUp> KRW
                  </strong>
                </CCallout>
              </CCol>
              <CCol sm="6">
                <CCallout color="info">
                  <strong className="text-muted">Profit</strong>
                  <br />
                  <strong className="h4">
                    <CountUp
                      end={0 < volumeList.todayProfit ? parseInt(volumeList.todayProfit) : 0}
                      delay={0}
                      decimals={0}
                      duration={1}
                      separator={','}
                      preserveValue={true}
                    >
                      {({ countUpRef }) => (
                        <span
                          ref={countUpRef}
                        />
                      )}
                    </CountUp> KRW
                  </strong>
                </CCallout>
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Volume;
