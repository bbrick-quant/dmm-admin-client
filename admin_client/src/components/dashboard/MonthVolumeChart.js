import React, {useEffect, useState} from 'react';
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";
import {CCard, CCol, CRow} from '@coreui/react';

import {useSelector} from 'react-redux';


const MonthVolumeChart = () => {

  const volumeList = useSelector(state => state.dmm.volumeChart);

  const [chartData, setChartData] = useState([]);
  const [chartTime, setChartTime] = useState(Date.now());

  useEffect(() => {
    let tmp = []
    for(let i=0, max=volumeList.length; i<max; i+=1) {
      if (0 === i) setChartTime(new Date(volumeList[i].createdAt).getTime())
      tmp.push(volumeList[i].volume)
    }
    setChartData(tmp)
  }, [volumeList]);

  const options = {
    title: {
      text: "Swap Volume (KRW)"
    },
    credits: {
      enabled: false
    },
    rangeSelector: {
      // https://codesandbox.io/s/jzjzr57jw?from-embed=&file=/demo.jsx
      // https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/stock/rangeselector/datagrouping/
      allButtonsEnabled: true,
      buttons: [
        {
          type: 'month',
          count: 3,
          text: 'Month',
          dataGrouping: {
            forced: true,
            units: [['day', [1]]]
          }
        },
      ],
      buttonTheme: {
        width: 60
      },
      selected: 2
    },

    series: [
      {
        name: 'Volume',
        pointStart: chartTime,
        pointInterval: 24 * 3600 * 1000, // one day
        // data: [1, 2, 1, 4, 3, 6, 7,],
        data: chartData,
        tooltip: {
          valueDecimals: 0
        },
        marker: {
          enabled: null, // auto
          radius: 3,
          lineWidth: 1,
          lineColor: '#FFFFFF'
        },
      }
    ]
  };

  return (
    <CCard>
      <CRow>
        <CCol>
          <CCard>
            <HighchartsReact
              highcharts={Highcharts}
              constructorType={"stockChart"}
              options={options}
            />
          </CCard>
        </CCol>
      </CRow>
    </CCard>
  )
}

export default MonthVolumeChart;
