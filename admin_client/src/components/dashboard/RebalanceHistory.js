import {CCard, CCardBody, CCardHeader, CCol, CDataTable} from "@coreui/react";
import React  from "react";
import {useSelector} from "react-redux";

import {customDate, handleDecimal, number} from '../../utils/common';

const SwapHistory = () => {

  const rebalanceHistory = useSelector(state => state.dmm.dashboardRebalanceHistoryList);

  const handleStatus = (status) => {
    if ('S' === status) return 'SUCCEED'
    if ('F' === status) return 'FAILED'
    if ('C' === status) return 'CANCELED'
    if ('P' === status) return 'PROCESSING'
  }

  return (
    <CCol lg={12} xl={6}>
      <CCard>
        <CCardHeader>
          <h4>Rebalance History</h4>
        </CCardHeader>
        <CCardBody>
          <CDataTable
            items={rebalanceHistory}
            fields={[
              {key: 'fromCoin', label: 'From'},
              {key: 'fromAmount', label: 'From Amount'},
              {key: 'toCoin', label: 'To'},
              {key: 'toAmount', label: 'To Amount'},
              {key: 'handleStatus', label: 'Status'},
              {key: 'completedPlan', label: 'Completed Plan'},
              {key: 'userName', label: 'User'},
              {key: 'createdAt', label: 'Created'},
              {key: 'updatedAt', label: 'Updated'},
            ]}
            scopedSlots = {{
              fromCoin: (val) => {
                return (
                  <td>
                    {val.fromCoin.toUpperCase()}
                  </td>
                )
              },
              toCoin: (val) => {
                return (
                  <td>
                    {val.toCoin.toUpperCase()}
                  </td>
                )
              },
              fromAmount: (val) => {
                return (
                  <td>
                    { number(handleDecimal(val.fromCoin, val.fromAmount)) }
                  </td>
                )
              },
              toAmount: (val) => {
                return (
                  <td>
                    { number(handleDecimal(val.toCoin, val.toAmount)) }
                  </td>
                )
              },
              handleStatus: (val) => {
                return (
                  <td>
                    { handleStatus(val.handleStatus) }
                  </td>
                )
              },
              createdAt: (val) => {
                return (
                  <td>
                    { customDate(val.createdAt, 'YYYY-MM-DD HH:mm:ss') }
                  </td>
                )
              },
              updatedAt: (val) => {
                return (
                  <td>
                    { customDate(val.updatedAt, 'YYYY-MM-DD HH:mm:ss') }
                  </td>
                )
              },
            }}
          />
        </CCardBody>
      </CCard>
    </CCol>
  )
}

export default SwapHistory;
