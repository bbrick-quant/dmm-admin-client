import React, {useState} from "react";
import {useSelector} from "react-redux";
import {CCard, CCardBody, CCardHeader, CFormGroup, CInputCheckbox, CLabel} from "@coreui/react";
import store from "src/store";
import {SELECT_INTERESTED_ASSET} from "src/store/accountBalance";

const InterestedAssetList = () => {
  const assetSymbols = useSelector(state => state.accountBalance.assetSymbols);
  const interestedAssets = useSelector(state => state.accountBalance.interestedAssets);

  return (
    <CCard>
      <CCardHeader>
        <h4 className="card-title mb-0">Asset List</h4>
      </CCardHeader>
      <CCardBody>
      {
        assetSymbols.map(val => {
          const keyname = `interested-asset-check-${val}`;
          const checked = interestedAssets.has(val);
          return (
            <CFormGroup
              key={val}
              variant="checkbox"
              className="checkbox"
              inline
            >
              <CInputCheckbox
                id={keyname}
                name="assetSymbolCheck"
                value={val}
                checked={checked}
                onChange={()=>{store.dispatch({type: SELECT_INTERESTED_ASSET, asset: val, flag: !checked})}}
              />
              <CLabel
                variant="checkbox"
                className="form-check-label"
                htmlFor={keyname}
              >
                {val}
              </CLabel>
            </CFormGroup>
          )
        })
      }
      </CCardBody>
    </CCard>
  )
}

export default InterestedAssetList;
