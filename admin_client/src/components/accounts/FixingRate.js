import {
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CDataTable,
  CFormGroup,
  CInputRadio,
  CLabel
} from "@coreui/react";
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";

const keyAssetList = ['BTC', 'ETH', 'KRW', 'USDT'];

const FixingRate = ({balanceScopedSlots, keyAsset, setKeyAsset}) => {
  const assetSymbols = useSelector(state => state.accountBalance.filteredAssetSymbol);
  const calcFixingRate = useSelector(state => state.accountBalance.calcFixingRate);

  const [fields, setFields] = useState([]);
  const [fixingRate, setFixingRate] = useState([]);

  useEffect(()=>{
    const getFixingRate = (asset, key)=> calcFixingRate?.[asset]?.[key] || 0;
    const fixingRateObject = {};

    for(let i=0, max=assetSymbols.length; i<max; i+=1){
      const asset = assetSymbols[i];

      fixingRateObject[asset] = getFixingRate(asset, keyAsset);
    }

    setFixingRate([fixingRateObject]);
  }, [assetSymbols, calcFixingRate]);

  useEffect(()=> {
    const fieldList = [];
    for(const key of assetSymbols) {
      fieldList.push({key, sorter: true, _classes: 'text-right'})
    }
    setFields(fieldList);
  }, [assetSymbols]);

  return (
    <CCard>
      <CCardHeader>
        <h4 className="card-title mb-0">
          Fixing Rate
        </h4>
      </CCardHeader>
      <CCardBody>
        <CDataTable
          items={fixingRate}
          fields={fields}
          scopedSlots={balanceScopedSlots}
          itemsPerPage={200}
        />
      </CCardBody>
      <CCardFooter>
        {
          keyAssetList.map((val, idx) => {
            const keyname = `asset-radio${idx}`
            return (
              <CFormGroup key={val} variant="custom-radio" inline>
                <CInputRadio
                  custom id={keyname}
                  name="inline-radios"
                  value="option1"
                  checked={keyAsset === val}
                  onChange={()=>{ setKeyAsset(val) }}
                />
                <CLabel variant="custom-checkbox" htmlFor={keyname}>{val}</CLabel>
              </CFormGroup>)
          })
        }
      </CCardFooter>
    </CCard>
  )
}

export default FixingRate;
