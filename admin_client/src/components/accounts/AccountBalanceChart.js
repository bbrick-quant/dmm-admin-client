import {CCard, CCardBody, CCardHeader, CCol, CRow} from "@coreui/react";
import React, {useEffect, useState} from "react";
import {CChartPie} from "@coreui/react-chartjs";
import {useSelector} from "react-redux";

const backgroundColor = [
  '#4e79a7', '#59a14f', '#9c755f', '#f28e2b', '#edc948',
  '#bab0ac', '#e15759', '#b07aa1', '#76b7b2', '#ff9da7',
  '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
  '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
];

const PieChart = React.memo(({labels, data}) => {
  return (<CChartPie
    datasets={[
      {
        backgroundColor,
        data,
      }
    ]}
    labels={labels}
    options={{
      aspectRatio: 1,
      animation: false,
      tooltips: {
        enabled: true
      },
    }}
  />)
});

const AccountBalanceChart = ({keyAsset}) => {
  const assetSymbols = useSelector(state => state.accountBalance.filteredAssetSymbol);
  const accounts = useSelector(state=> state.accountBalance.accounts);
  const calcFixingRate = useSelector(state => state.accountBalance.calcFixingRate);
  const exchangeSymbols = useSelector(state => state.accountBalance.exchangeSymbols);

  const [positionByAsset, setPositionByAsset] = useState([]);
  const [positionByExchange, setPositionByExchange] = useState([]);

  useEffect(()=>{
    const getFixingRate = (asset, key)=> calcFixingRate?.[asset]?.[key] || 0;

    // 차트용
    const totalByBTCArray = [];
    const exchangePosition = [];

    for(let i=0, max=assetSymbols.length; i<max; i+=1){
      totalByBTCArray[i] = 0;
    }

    for(let i=0, max=accounts.length; i<max; i+=1){
      const account = accounts[i];

      exchangePosition[i] = 0;
      assetSymbols.forEach((val, idx) => {
        const size = +(account[val] || 0);

        totalByBTCArray[idx] += size * getFixingRate(val, keyAsset);
        exchangePosition[i] += size * getFixingRate(val, keyAsset);
      })
    }

    if(JSON.stringify(positionByAsset) !== JSON.stringify(totalByBTCArray)){
      setPositionByAsset(totalByBTCArray);
    }
    if(JSON.stringify(positionByExchange) !== JSON.stringify(exchangePosition)){
      setPositionByExchange(exchangePosition);
    }
  }, [assetSymbols, accounts, calcFixingRate, keyAsset])

  return (
    <CRow>
      <CCol xl="12" lg="6">
        <CCard>
          <CCardHeader>
            <h4 className="card-title mb-0">Position by Assets</h4>
          </CCardHeader>
          <CCardBody
          >
            <PieChart
              labels={assetSymbols}
              data={positionByAsset}
            />
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xl="12" lg="6">
        <CCard>
          <CCardHeader>
            <h4 className="card-title mb-0">Position By Exchanges</h4>
          </CCardHeader>
          <CCardBody
          >
            <PieChart
              labels={exchangeSymbols}
              data={positionByExchange}
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
};

export default AccountBalanceChart;
