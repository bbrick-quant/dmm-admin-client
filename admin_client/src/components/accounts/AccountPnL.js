import FixingRate from "./FixingRate";
import {CCard, CCardBody, CCardHeader, CCol, CDataTable} from "@coreui/react";
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";

const AccountPnL = ({fieldsWithExchange, balanceScopedSlots, keyAsset}) => {
  const assetSymbols = useSelector(state => state.accountBalance.filteredAssetSymbol);
  const total = useSelector(state => state.accountBalance.total);
  const initialSet = useSelector(state => state.accountBalance.initialSet);
  const debtSet = useSelector(state => state.accountBalance.debtSet);
  const calcFixingRate = useSelector(state => state.accountBalance.calcFixingRate);

  const [pnl, setPnL] = useState([]);

  useEffect(()=>{
    const getFixingRate = (asset, key)=> calcFixingRate?.[asset]?.[key] || 0;

    const totalPnL = {exchange: 'Total PnL'};
    const totalPnlInBTC = {exchange: `Total Pnl In ${keyAsset}`}
    const totalAll = {exchange: 'Total All'};

    for(let i=0, max=assetSymbols.length; i<max; i+=1){
      const asset = assetSymbols[i];
      totalPnL[asset] = 0;
    }
    totalAll.BTC = 0;

    for(const asset of assetSymbols) {
      totalPnL[asset] = total[asset] - initialSet[asset];
      totalPnlInBTC[asset] = totalPnL[asset] * getFixingRate(asset, keyAsset);

      totalAll.BTC += totalPnlInBTC[asset] || 0;
    }

    setPnL([
      {exchange: 'Debt', ...debtSet},
      {exchange: 'Initial Set', ...initialSet},
      {exchange: 'Total', ...total},
      totalPnL,
      totalPnlInBTC,
      totalAll,
    ]);
  }, [assetSymbols, total, initialSet, debtSet, calcFixingRate]);
  return (
    <CCard>
      <CCardHeader>
        <h4 className="card-title mb-0">PnL</h4>
      </CCardHeader>
      <CCardBody>
        <CDataTable
          items={pnl}
          fields={fieldsWithExchange}
          scopedSlots={balanceScopedSlots}
          itemsPerPage={200}
        />
      </CCardBody>
    </CCard>
  );
};

export default AccountPnL;
