import {CCard, CCardBody, CCardHeader, CCol, CDataTable} from "@coreui/react";
import React, { useState, useEffect } from "react";
import {useSelector} from "react-redux";

const AccountBalance = ({ keyAsset, fieldsWithExchange, balanceScopedSlots }) => {
  const accounts = useSelector(state=> state.accountBalance.accounts);
  const assetSymbols = useSelector(state => state.accountBalance.filteredAssetSymbol);
  const calcFixingRate = useSelector(state => state.accountBalance.calcFixingRate);
  const total = useSelector(state => state.accountBalance.total);

  const [balance, setBalance] = useState([]);

  useEffect(() => {
    const getFixingRate = (asset, key)=> calcFixingRate?.[asset]?.[key] || 0;
    const totalInBTC = {exchange: `Total In ${keyAsset}`};

    for(let i=0, max=assetSymbols.length; i<max; i+=1){
      const asset = assetSymbols[i];
      totalInBTC[asset] = 0;
    }

    for(let i=0, max=accounts.length; i<max; i+=1){
      const account = accounts[i];

      assetSymbols.forEach((val, idx) => {
        const size = +(account[val] || 0);

        totalInBTC[val] += size * getFixingRate(val, keyAsset);
      })
    }

    setBalance([
      ...accounts,
      {exchange: 'Total', ...total},
      totalInBTC,
    ])
  }, [accounts, assetSymbols, calcFixingRate, total]);

  return (
    <CCard>
      <CCardHeader>
        <h4 className="card-title mb-0">Account Balance</h4>
      </CCardHeader>
      <CCardBody>
        <CDataTable
          items={balance}
          fields={fieldsWithExchange}
          scopedSlots={balanceScopedSlots}
          itemsPerPage={200}
        />
      </CCardBody>
    </CCard>
  )
}

export default AccountBalance;
