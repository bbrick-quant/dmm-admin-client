import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CSelect,
  CLabel,
  CInput,
  CRow,
  CCol,
  CDataTable,
} from "@coreui/react";
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {getTransferHistory, postTransfer} from "src/actions/fund";

const dateTimeFormat = new Intl.DateTimeFormat('ko-KR', {
  // year: 'numeric', month: 'numeric', day: 'numeric',
  // hour: 'numeric', minute: 'numeric', second: 'numeric',
  hour12: false,
  dateStyle: 'medium', timeStyle: 'short'
}).format;

const Transfer = ({accountId, asset, show, setModal}) => {
  const apiKeyList = useSelector(state => state.accountBalance.apiKeyList);
  const transferHistories = useSelector(state => state.transfer.histories);

  const [fromAccount, setFromAccount] = useState('');
  const [toAccountId, setToAccountId] = useState(0);
  const [size, setSize] = useState(0);
  const [description, setDescription] = useState('');
  const [canTransfer, setTransfer] = useState(true);
  const [errorMsg, setErrorMsg] = useState('');

  useEffect(()=>{
    const account = apiKeyList.find((val) => val.id === accountId)
    if(account) {
      setFromAccount(`${account.fundName} - ${account.accountName}`);
    }
  }, [apiKeyList, accountId])

  useEffect(()=>{
    const filteredApiKeyList = apiKeyList.filter(val => val.id !== accountId);
    if(filteredApiKeyList.length > 0) {
      setToAccountId(filteredApiKeyList[0].id);
    }
  }, [fromAccount ,apiKeyList]);

  useEffect(()=>{
    if(show) {
      setSize(0);
      setDescription('');
      setErrorMsg('');

      getTransferHistory();
    }
  }, [show])

  const getAccountName = (id) => {
    const account = apiKeyList.find(val => val.id === id);
    if(!account) return id;

    return `${account.fundName} - ${account.accountName}`;
  }

  const onTransfer = async () => {
    setTransfer(false);
    setErrorMsg('');
    try {
      await postTransfer({
        size,
        currency: asset,
        description,
        from_account: accountId,
        to_account: toAccountId,
      });

      alert(`Success Transfer\n\n${asset} - ${size}\nFrom : ${getAccountName(accountId)}\nTo : ${getAccountName(toAccountId)}`);

      setDescription('');
      setSize(0);

      getTransferHistory();
    }
    catch(e) {
      setErrorMsg(e?.response?.data ? JSON.stringify(e?.response?.data) : 'error');
    } finally {
      setTransfer(true);
    }
  };

  return (
    <CModal
      show={show}
      onClose={() => setModal(!show)}
      size="lg"
    >
      <CModalHeader closeButton>
        <CModalTitle>Transfer</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CRow>
          <CCol md={2}>
            <h3>Asset</h3>
          </CCol>
          <CCol>
            {asset}
          </CCol>
        </CRow>
        <CRow>
          <CCol md={2}>
            <h3>From</h3>
          </CCol>
          <CCol>
            {fromAccount}
          </CCol>
        </CRow>
        <CRow>
          <CCol md={2}>
            <h3>To</h3>
          </CCol>
          <CCol>
            <CSelect
              custom
              name="ccmonth"
              id="ccmonth"
              onChange={(e)=>{setToAccountId(+e.target.value)}}
            >
              {
                apiKeyList
                  .filter(val => val.id !== accountId)
                  .map(val => (
                  <option
                    key={val.id}
                    value={val.id}
                  >
                    {`${val.fundName} - ${val.accountName}`}
                  </option>
                ))
              }
            </CSelect>
          </CCol>
        </CRow>
        <CRow>
          <CCol md={2}>
            <h3>Size</h3>
          </CCol>
          <CCol>
            <CInput
              type="number"
              value={size}
              min="0"
              onChange={(e)=>{setSize(e.target.value)}}
            />
          </CCol>
        </CRow>
        <CRow>
          <CCol md={2}>
            <h3>Desc</h3>
          </CCol>
          <CCol>
            <CInput
              value={description}
              onChange={(e)=>{setDescription(e.target.value)}}
            />
          </CCol>
        </CRow>
        <CRow
          style={{
            justifyContent: 'center',
          }}
        >
          <span
            style={{
              color: 'red'
            }}
          >
          {errorMsg}
          </span>
        </CRow>
        <CRow
          style={{justifyContent: 'center'}}
        >
          <CButton
            color="primary"
            onClick={() => {
              onTransfer();
            }}
            active={canTransfer}
            disabled={!canTransfer}
          >
            Transfer
          </CButton>
        </CRow>
        <CRow>
          <CDataTable
            items={transferHistories}
            fields={[
              {key: 'createdAt', label: 'Date'},
              {key: 'fromAccount', label: 'From'},
              {key: 'toAccount', label: 'To'},
              {key: 'currency', label: 'Asset'},
              'size',
              {key: 'description', label: 'Desc'},
            ]}
            scopedSlots={{
              fromAccount: (value)=> {
                const account = apiKeyList.find(val => val.id === value.fromAccount);
                if(!account) { return <td>{value.fromAccount}</td>}
                return (
                  <td>
                    {account.fundName}
                    <br/>
                    {account.accountName}
                  </td>
                );
              },
              toAccount: (value)=> {
                const account = apiKeyList.find(val => val.id === value.toAccount);
                if(!account) { return <td>{value.toAccount}</td>}
                return (
                  <td>
                    {account.fundName}
                    <br/>
                    {account.accountName}
                  </td>
                );
              },
              createdAt: (value) => {
                return (
                  <td>
                    {dateTimeFormat(new Date(value.createdAt))}
                  </td>
                )
              }
            }}
          />
        </CRow>
      </CModalBody>
      <CModalFooter>
        <CButton color="secondary" onClick={() => setModal(!show)}>Close</CButton>
      </CModalFooter>
    </CModal>
  )
}

export default Transfer;
