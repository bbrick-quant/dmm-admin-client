import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CSwitch,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CInput,
} from '@coreui/react';

import React, {useState} from 'react';
import {useSelector} from "react-redux";

import {
  customDate,
  getUsername,
  viewPairFormat,
} from "../../utils/common";

import axios from '../../axios';
import {
  getQuoteList,
} from '../../actions/dmm';


const QuoteCtrl = props => {
  const quoteList = useSelector(state => state.dmm.quoteList);

  const [pair, setPair] = useState('');

  // Quote Control Modal
  const [quoteCtrlModal, setQuoteCtrlModal] = useState(false);
  const [quoteStatus, setQuoteStatus] = useState('');
  const [updateQuoteParam, setUpdateQuoteParam] = useState({});

  // Quote Ratio Modal
  const [changeRatioModal, setChangeRatioModal] = useState(false);
  const [bidPercent, setBidPercent] = useState(0);
  const [askPercent, setAskPercent] = useState(0);
  const [changeRatioParam, setChangeRatioParam] = useState({});


  const inputReset = () => {
    Array.from(document.querySelectorAll('input')).forEach(
      input => (input.value = '')
    );
  };

  const handleQuoteStatus = (item) => {
    setPair(item.swapPair)
    setQuoteStatus(item.status)
    setQuoteCtrlModal(!quoteCtrlModal)

    const param = {
      "command": "quote_on_off",
      "data": 'on' === item.status ? 'off':'on',
      "swapPair": item.swapPair,
    }
    setUpdateQuoteParam(param)
  }

  const handleQuoteRatio = (item) => {
    if (bidPercent === 0 || askPercent === 0) {
      alert(`Buy, Sell 수수료를 입력하세요`)
      return;
    }
    if (1 !== Number(bidPercent)+Number(askPercent)) {
      alert(`Buy, Sell 수수료의 합을 1로 만들어주세요`)
      return;
    }

    setPair(item.swapPair)
    setChangeRatioModal(!quoteCtrlModal)

    const param = {
      "command": 'bid_ask_margin',
      "data": {
        'bidPercent': bidPercent,
        'askPercent': askPercent,
      },
      "swapPair": item.swapPair,
    }
    setChangeRatioParam(param)
  }

  async function reqAfterProcess(id) {
    props.setLoaded(true);

    const reqInfo = await axios.get(`/api/v1/dmm/control/quote/${id}/`);

    if (200 === reqInfo.status) {
      const reqStatus = reqInfo.data['detailInfo'].handleStatus;

      if ('S' === reqStatus) {
        alert(`변경되었습니다`)
        getQuoteList()
      }
      if ('F' === reqStatus) alert(`요청이 실패되었습니다 관리자에게 문의하세요`)
    }
    else {
      alert(`Quote 변경 요청 시 오류 발생 관리자에게 문의하세요`)
    }

    props.setLoaded(false);
  }

  async function reqUpdateQuoteStatus() {
    const res = await axios.post('/api/v1/dmm/control/update_quote/', {userName: getUsername(), reqData: updateQuoteParam});

    if (201 === res.status) {
      setQuoteCtrlModal(!quoteCtrlModal)
      reqAfterProcess(res.data.id)
    }
    else {
      alert(`서버 오류 관리자에게 문의하세요`)
    }
    setQuoteCtrlModal(!quoteCtrlModal)
  }

  async function reqChangeRatio() {
    const res = await axios.post('/api/v1/dmm/control/update_quote/', {userName: getUsername(), reqData: changeRatioParam});

    if (201 === res.status) {
      setChangeRatioModal(!changeRatioModal)
      reqAfterProcess(res.data.id)
      inputReset();
    }
    else {
      alert(`서버 오류 관리자에게 문의하세요`)
    }
    setChangeRatioModal(!changeRatioModal)
  }


  return (
    <CCol xl="11" lg="12">
      <CCard>
        <CCardHeader>
          <h4 className="card-title mb-0">Quote Control</h4>
        </CCardHeader>
        <CCardBody>
          <table className="table table-dark table-hover table-outline mb-0 d-none d-sm-table" style={{backgroundColor:'#000000'}}>
            <thead>
            <tr>
              <th>Pair</th>
              <th colSpan="2">status</th>
              <th>Buy Fee Rate</th>
              <th>Sell Fee Rate</th>
              <th>Change Ratio</th>
              <th>Updated at</th>
            </tr>
            </thead>
            <tbody>
            {
              quoteList.map((val, idx) => {
                return (
                  <tr key={idx}>
                    <td>{viewPairFormat(val.swapPair)}</td>
                    <td className="text-left">
                      <CSwitch
                        className='mx-1'
                        variant='3d'
                        color='success'
                        checked={'on' === val.status}
                        onChange={() => handleQuoteStatus(val)}
                      />
                    </td>
                    <td>
                      {val.status === 'on' ?
                        <img alt="on" src="/img/heartbeat_on.gif" style={{width:'170px', height: '40px', paddingRight:'12px'}}/>
                        :
                        <img alt="off" src="/img/heartbeat_off.gif" style={{width:'170px', height: '40px', paddingRight:'12px'}}/>
                      }
                    </td>
                    <td>
                      <CCol xs="12" md="7">
                        <CInput
                          // value={val.bidPercent}
                          placeholder={ parseFloat(val.bidPercent).toFixed(2) }
                          onChange={e => setBidPercent(e.target.value)}
                        />
                      </CCol>
                    </td>
                    <td>
                      <CCol xs="12" md="7">
                        <CInput
                          // value={val.askPercent}
                          placeholder={ parseFloat(val.askPercent).toFixed(2) }
                          onChange={e => setAskPercent(e.target.value)}
                        />
                      </CCol>
                    </td>
                    <td>
                      <CCol col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
                        <CButton onClick={() => handleQuoteRatio(val)} block color="warning">Request</CButton>
                      </CCol>
                    </td>
                    <td>{customDate(val.updatedAt, 'YYYY-MM-DD HH:mm:ss')}</td>
                  </tr>
                )
              })
            }
            </tbody>
          </table>
        </CCardBody>
      </CCard>

      <CModal
        show={quoteCtrlModal}
        onClose={() => setQuoteCtrlModal(!quoteCtrlModal)}
        color="danger"
      >
        <CModalHeader closeButton>
          <CModalTitle>Danger</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {`${viewPairFormat(pair)} quote를 ${'on'===quoteStatus ? '"중지"' : '"시작"'}하시겠습니까?`}
        </CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={() => reqUpdateQuoteStatus()}>Confirm</CButton>{' '}
          <CButton color="secondary" onClick={() => setQuoteCtrlModal(!quoteCtrlModal)}>Cancel</CButton>
        </CModalFooter>
      </CModal>

      <CModal
        show={changeRatioModal}
        onClose={() => setChangeRatioModal(!changeRatioModal)}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>Warning</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {viewPairFormat(pair)} 수수료 비율을 다음과 같이 변경하시겠습니까?  <br/><br/>
          {`Buy: ${bidPercent}%`}  <br/>
          {`Sell: ${askPercent}%`} <br/>
        </CModalBody>
        <CModalFooter>
          <CButton color="warning" onClick={() => reqChangeRatio()}>Confirm</CButton>{' '}
          <CButton color="secondary" onClick={() => setChangeRatioModal(!changeRatioModal)}>Cancel</CButton>
        </CModalFooter>
      </CModal>

    </CCol>
  )
}

export default QuoteCtrl;
