import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CInput,
  CFormGroup,
  CFormText,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu, CDropdownItem,
} from '@coreui/react';

import React, { useState } from 'react'
import {useSelector} from "react-redux";
import {getUsername} from '../../utils/common';
import axios from '../../axios';


const ManualRebalanceCtrl = () => {
  const rebalanceList = useSelector(state => state.dmm.rebalanceList);

  const [mShow, setMShow] = useState(false);
  const [fromCoin, setFromCoin] = useState('Select');
  const [toCoin, setToCoin] = useState('Select');
  const [fromAmount, setFromAmount] = useState(0);
  const [param, setParam] = useState({});


  const initData = () => {
    inputReset()
    setFromCoin('Select')
    setToCoin('Select')
    setParam({})
  }

  const inputReset = () => {
    Array.from(document.querySelectorAll('input')).forEach(
      input => (input.value = '')
    );
  };

  const handleRebalance = () => {
    if (fromCoin === 'Select' || toCoin === 'Select' || fromAmount === 0) {
      alert('입력값을 확인해주세요')
      return;
    }
    setMShow(!mShow)
    const param = {
      'fromCoin': fromCoin,
      'toCoin': toCoin,
      'fromAmount': fromAmount,
      'userName': getUsername(),
    }
    setParam(param)
  }

  async function reqRebalance() {
    const res = await axios.post('/api/v1/dmm/control/req_rebalance/', param);

    if (201 === res.status) {
      alert(`Rebalance 요청이 완료 되었습니다`)
      initData()
    }
    else {
      alert(`서버 오류 관리자에게 문의하세요`)
    }
    setMShow(!mShow)
  }

  return (
    <CCol xl="11" lg="12">
      <CCard>
        <CCardHeader>
          <h4 className="card-title mb-0">Manual Rebalance Control</h4>
        </CCardHeader>
        <CCardBody>
          <CFormGroup row>
            <CCol xs="12" md="2" style={{maxWidth: '11%'}}>
              <CFormText className="help-block"><h6>From</h6></CFormText>
              <CDropdown className="m-1">
                <CDropdownToggle style={{boxShadow: '0 1px 1px 0 rgb(60 75 100 / 14%), 0 2px 1px -1px rgb(60 75 100 / 12%), 0 1px 3px 0 rgb(60 75 100 / 20%)'}}>
                  {fromCoin.toUpperCase()}
                </CDropdownToggle>
                <CDropdownMenu>
                  {
                    rebalanceList.map((item, idx) => {
                      return (
                        <CDropdownItem onClick={() => setFromCoin(item.coin)} key={idx}>{item.coin.toUpperCase()}</CDropdownItem>
                      )
                    })
                  }
                </CDropdownMenu>
              </CDropdown>
            </CCol>

            <CCol xs="12" md="2" style={{maxWidth: '11%'}}>
              <CFormText className="help-block"><h6>To</h6></CFormText>
              <CDropdown className="m-1">
                <CDropdownToggle style={{boxShadow: '0 1px 1px 0 rgb(60 75 100 / 14%), 0 2px 1px -1px rgb(60 75 100 / 12%), 0 1px 3px 0 rgb(60 75 100 / 20%)'}}>
                  {toCoin.toUpperCase()}
                </CDropdownToggle>
                <CDropdownMenu>
                  {
                    rebalanceList.map((item, idx) => {
                      return (
                        <CDropdownItem onClick={() => setToCoin(item.coin)} key={idx}>{item.coin.toUpperCase()}</CDropdownItem>
                      )
                    })
                  }
                </CDropdownMenu>
              </CDropdown>
            </CCol>

            <CCol xs="12" md="2" style={{maxWidth: '21%'}}>
              <CFormText className="help-block"><h6>From Amount</h6></CFormText>
              <CInput
                placeholder="amount"
                onChange={e => setFromAmount(e.target.value)}
              />
            </CCol>
            <CCol xs="12" md="2" style={{maxWidth: '21%', marginTop: '26px'}}>
              <CButton onClick={() => handleRebalance()} style={{fontWeight: '500'}} color="warning">Request</CButton>
            </CCol>

          </CFormGroup>
        </CCardBody>
      </CCard>

      <CModal
        show={mShow}
        onClose={() => setMShow(!mShow)}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>Warning</CModalTitle>
        </CModalHeader>
        <CModalBody>
          다음과 같이 Rebalance 요청을 하시겠습니까? <br/><br/>
          fromCoin: {fromCoin.toUpperCase()} <br/>
          fromAmount: {fromAmount} <br/><br/>
          toCoin: {toCoin.toUpperCase()} <br/>
        </CModalBody>
        <CModalFooter>
          <CButton onClick={() => reqRebalance()} color="warning">Confirm</CButton>{' '}
          <CButton color="secondary" onClick={() => setMShow(!mShow)}>Cancel</CButton>
        </CModalFooter>
      </CModal>
    </CCol>
  )
}

export default ManualRebalanceCtrl;
