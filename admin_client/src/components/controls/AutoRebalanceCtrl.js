import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CSwitch,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CInput,
  CInputGroup,
  CInputGroupAppend,
} from '@coreui/react';

import React, { useState } from 'react'
import {useSelector} from "react-redux";
import {customDate, viewPairFormat} from '../../utils/common';
import axios from '../../axios';
import {getRebalanceList} from '../../actions/dmm';


const AutoRebalanceCtrl = () => {
  const rebalanceList = useSelector(state => state.dmm.rebalanceList);

  // rebalance Modal
  const [rebalanceCtrlModal, setRebalanceCtrlModal] = useState(false);
  const [coin, setCoin] = useState('');
  const [rebalanceStatus, setRebalanceStatus] = useState('');

  const [param, setParam] = useState({});
  const [thresholdModal, setThresholdModal] = useState(false);
  const [thresholdAmount, setThresholdAmount] = useState(0);
  const [currenctThresholdAmount, setCurrenctThresholdAmount] = useState(0);

  const [percentModal, setPercentModal] = useState(false);
  const [targetPercent, setTargetPercent] = useState(0);


  const handleTargetPercent = (item) => {
    setCoin(item.coin)
    setPercentModal(!percentModal)

    const param = {
      'targetComparePercent': targetPercent,
      'exName': item.exName,
      'coin': item.coin,
    }
    setParam(param)
  }

  const handleRebalanceStatus = (item) => {
    setCoin(item.coin)
    setRebalanceStatus(item.status)
    setRebalanceCtrlModal(!rebalanceCtrlModal)

    const param = {
      'status': 'on'===item.status? 'off' : 'on',
      'exName': item.exName,
      'coin': item.coin,
    }
    setParam(param)
  }

  const handleThreshold = (item) => {
    setCoin(item.coin)
    setCurrenctThresholdAmount(item.threshholdMin)
    setThresholdModal(!thresholdModal)

    const param = {
      'threshholdMin': thresholdAmount,
      'exName': item.exName,
      'coin': item.coin,
    }
    setParam(param)
  }

  async function updateAutoRebalance() {
    const res = await axios.patch('/api/v1/dmm/control/update_rebalance/', param);

    if (200 === res.status) {
      initData()
      getRebalanceList()
      alert(`변경 되었습니다`)
    }
    else {
      alert(`서버 오류 관리자에게 문의하세요`)
    }
  }

  const initData = () => {
    inputReset()
    setParam({})
    setRebalanceCtrlModal(false)
    setThresholdModal(false)
    setPercentModal(false)
  }

  const inputReset = () => {
    Array.from(document.querySelectorAll('input')).forEach(
      input => (input.value = '')
    );
  };

  return (
    <CCol xl="11" lg="12">

      {/* Modal START */}
      <CModal
        show={rebalanceCtrlModal}
        onClose={() => setRebalanceCtrlModal(!rebalanceCtrlModal)}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>Warning</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {`${viewPairFormat(coin)} Auto Rebalance 를 ${'on'===rebalanceStatus ? '"중지"' : '"시작"'}하시겠습니까?`}
        </CModalBody>
        <CModalFooter>
          <CButton color="warning" onClick={() => updateAutoRebalance()}>Confirm</CButton>{' '}
          <CButton color="secondary" onClick={() => setRebalanceCtrlModal(!rebalanceCtrlModal)}>Cancel</CButton>
        </CModalFooter>
      </CModal>

      <CModal
        show={thresholdModal}
        onClose={() => setThresholdModal(!thresholdModal)}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>Warning</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {`${viewPairFormat(coin)} 코인의 Auto Rebalance 임계치를 다음과 같이 변경하시겠습니까?`} <br/><br/>
          Before: {currenctThresholdAmount} {viewPairFormat(coin)} <br/>
          After: {thresholdAmount} {viewPairFormat(coin)}
        </CModalBody>
        <CModalFooter>
          <CButton color="warning" onClick={() => updateAutoRebalance()}>Confirm</CButton>{' '}
          <CButton color="secondary" onClick={() => setThresholdModal(!thresholdModal)}>Cancel</CButton>
        </CModalFooter>
      </CModal>

      <CModal
        show={percentModal}
        onClose={() => setPercentModal(!percentModal)}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>Warning</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {`타겟 대비 ${targetPercent}% (${viewPairFormat(coin)}) 유지하시겠습니까?`} <br/><br/>
        </CModalBody>
        <CModalFooter>
          <CButton color="warning" onClick={() => updateAutoRebalance()}>Confirm</CButton>{' '}
          <CButton color="secondary" onClick={() => setPercentModal(!percentModal)}>Cancel</CButton>
        </CModalFooter>
      </CModal>
      {/* Modal END */}

      <CCard>
        <CCardHeader>
          <h4 className="card-title mb-0">Auto Rebalance Control</h4>
        </CCardHeader>
        <CCardBody>
          <table className="table table-dark table-hover table-outline mb-0 d-none d-sm-table" style={{backgroundColor:'#000000'}}>
            <thead>
            <tr>
              <th>Coin</th>
              <th colSpan="2">status</th>
              <th>Threshhold Min</th>
              <th>Target %</th>
              <th>Updated at</th>
            </tr>
            </thead>
            <tbody>
            {
              rebalanceList.map((item, idx) => {
                return (
                  <tr key={idx}>
                    <td>{item.coin.toUpperCase()}</td>
                    <td className="text-left">
                      <CSwitch
                        className='mx-1'
                        variant='3d'
                        color='success'
                        checked={'on' === item.status}
                        onChange={() => handleRebalanceStatus(item)}
                      />
                    </td>
                    <td>
                      {item.status === 'on' ?
                        <img alt="on" src="/img/heartbeat_on.gif" style={{width:'170px', height: '40px', paddingRight:'12px'}}/>
                        :
                        <img alt="off" src="/img/heartbeat_off.gif" style={{width:'170px', height: '40px', paddingRight:'12px'}}/>
                      }
                    </td>
                    <td>
                      <CCol xs="12" md="10">
                        <CInputGroup>
                          <CInput
                            // value={val.bid_percent}
                            placeholder={ parseFloat(item.threshholdMin).toFixed(2) }
                            onChange={e => setThresholdAmount(e.target.value)}
                          />
                          <CInputGroupAppend>
                            <CButton onClick={() => handleThreshold(item)} color="warning">save</CButton>
                          </CInputGroupAppend>
                        </CInputGroup>
                      </CCol>
                    </td>
                    <td>
                      <CCol xs="12" md="10">
                        <CInputGroup>
                          <CInput
                            // value={val.bid_percent}
                            placeholder={ parseInt(item.targetComparePercent) }
                            onChange={e => setTargetPercent(e.target.value)}
                          />
                          <CInputGroupAppend>
                            <CButton onClick={() => handleTargetPercent(item)} color="warning">save</CButton>
                          </CInputGroupAppend>
                        </CInputGroup>
                      </CCol>
                    </td>
                    <td>{customDate(item.updatedAt, 'YYYY-MM-DD HH:mm:ss')}</td>
                  </tr>
                )
              })
            }
            </tbody>
          </table>
        </CCardBody>
      </CCard>
    </CCol>


  )
}

export default AutoRebalanceCtrl;
