#!/bin/sh

#cd /Users/ocm/quant/dmm-admin-client/admin_client

echo "================================"
echo "docker-compose .."
echo "================================"
docker-compose -f docker-compose_test.yml build --force-rm

echo "================================"
echo "docker save .."
echo "================================"
docker save -o /Users/ocm/shell/dmmAC.tar dmm-admin-client

REMOTE_HOST=ubuntu@3.35.247.111
CERT_FILE=bbrick-aws1.pem
SERVER_FILE_DIR=/home/ubuntu/work/file
SERVER_SHELL_DIR=/home/ubuntu/work/shell

echo "================================"
echo "put dmmAC.tar to server .. $SERVER_FILE_DIR "
echo "================================"
cd /Users/ocm/shell
scp -i ~/.ssh/$CERT_FILE dmmAC.tar $REMOTE_HOST:$SERVER_FILE_DIR

echo "================================"
echo " restart dmm-client-docker "
echo "================================"

ssh -t -i ~/.ssh/$CERT_FILE -t $REMOTE_HOST "cd $SERVER_SHELL_DIR; ./dmm_admin_client_restart.sh "
